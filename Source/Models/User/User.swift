//
//  User.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RxSwift
import SwiftyJSON
import RealmSwift

protocol UserProtocol {
    var uuid                : String { get }
    var firstName           : String { get }
    var lastName            : String { get }
    var email               : String { get }
    var phone               : String { get }
    var largeImagePath      : String { get }
    var thumbnailImagePath  : String { get }
}

final class User: Hashable, Equatable, Model, UserProtocol {
    let uuid                : String
    var firstName           : String
    var lastName            : String
    var email               : String
    var phone               : String
    var largeImagePath      : String
    var thumbnailImagePath  : String
    
    //MARK: - Init, Deinit
    
    required init(json: SwiftyJSON.JSON) {
        self.uuid = json["login"]["uuid"].stringValue
        self.firstName = json["name"]["first"].stringValue.capitalized
        self.lastName = json["name"]["last"].stringValue.capitalized
        self.email = json["email"].stringValue
        self.phone = json["phone"].stringValue
        
        let picture = json["picture"]
        self.largeImagePath = picture["large"].stringValue
        self.thumbnailImagePath = picture["thumbnail"].stringValue
    }
    
    init(object: UserObject) {
        self.firstName = object.firstName
        self.lastName = object.lastName
        self.email = object.email
        self.uuid = object.uuid
        self.phone = object.phone
        self.largeImagePath = object.largeImagePath
        self.thumbnailImagePath = object.thumbnailImagePath
    }
    
//    deinit {
//        print(" -----> User \(self.firstName) dinit")
//    }
    
    //MARK: - Method Public
    
    public func update(with box: UserBoxRxProtocol) {
        self.firstName = box.firstName.value ?? self.firstName
        self.lastName = box.lastName.value ??  self.lastName
        self.email = box.email.value ?? self.email
        self.phone = box.phone.value ?? self.phone
        self.largeImagePath = box.largeImagePath.value ?? self.largeImagePath
        self.thumbnailImagePath = box.thumbnailImagePath.value ?? self.thumbnailImagePath
    }
    
    //MARK: - Hashable, Equatable
    
    var hashValue: Int {
        return uuid.hashValue
    }
    
    static func ==(lhs: User, rhs: User) -> Bool {
        return lhs.uuid.hashValue == rhs.uuid.hashValue &&
        lhs.firstName == rhs.firstName &&
        lhs.firstName == rhs.firstName &&
        lhs.lastName == rhs.lastName &&
        lhs.email == rhs.email &&
        lhs.phone == rhs.phone &&
        lhs.largeImagePath == rhs.largeImagePath &&
        lhs.thumbnailImagePath == rhs.thumbnailImagePath
    }
}

//MARK: - Stored Model Object

final class UserObject: Object {
    @objc dynamic var uuid                  = ""
    @objc dynamic var firstName             = ""
    @objc dynamic var lastName              = ""
    @objc dynamic var email                 = ""
    @objc dynamic var phone                 = ""
    @objc dynamic var largeImagePath        = ""
    @objc dynamic var thumbnailImagePath    = ""
    
    override static func primaryKey() -> String? {
        return "uuid"
    }

    convenience init(user: User) {
        self.init()
        self.uuid = user.uuid
        self.firstName = user.firstName
        self.lastName = user.lastName
        self.email = user.email
        self.phone = user.phone
        self.largeImagePath = user.largeImagePath
        self.thumbnailImagePath = user.thumbnailImagePath
    }
}

protocol UserBoxRxProtocol {
    var uuid:                   String { get }
    var firstName:              Variable<String?> { get }
    var lastName:               Variable<String?> { get }
    var email:                  Variable<String?> { get }
    var phone:                  Variable<String?> { get }
    var largeImagePath:         Variable<String?> { get }
    var thumbnailImagePath:     Variable<String?> { get }
}

class UserBox: UserBoxRxProtocol {
    let uuid:               String
    let firstName           = Variable<String?>(nil)
    let lastName            = Variable<String?>(nil)
    let email               = Variable<String?>(nil)
    let phone               = Variable<String?>(nil)
    let largeImagePath      = Variable<String?>(nil)
    let thumbnailImagePath  = Variable<String?>(nil)
    
    required init(user: User) {
        self.uuid = user.uuid
    }
    
//    deinit {
//        print(" -----> UserBox \(self.firstName) dinit")
//    }
}
