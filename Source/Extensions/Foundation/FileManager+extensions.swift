//
//  FileManager+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 23.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

extension FileManager {

    static func write(data: Data, path: URL) -> Bool {
        guard data.count > 0 && path.isFileURL else {
            return false
        }
        
        let directoryPath = path.deletingLastPathComponent()
        
        guard
            !FileManager.default.fileExists(atPath: directoryPath.path) &&
            create(directoryPath: directoryPath)
        else { return false }
        
        do {
            try data.write(to: path, options: .atomic)
            return true
        } catch {
            print("Error when saving data at location: \(path.absoluteString). The error was: \(error)")
            return false
        }
    }
    
    static func create(directoryPath path: URL) -> Bool {
        guard path.isFileURL else { return false }
        
        do {
            try FileManager.default.createDirectory(at: path, withIntermediateDirectories: true, attributes: nil)
            return true
        } catch {
            print("Error when creating a directory at location: \(path.absoluteString). The error was: \(error)")
            return false
        }
    }
    
    static func retrieve(dataPath path: URL) -> Data? {
        guard path.isFileURL else { return nil }
        
        do {
            return try Data(contentsOf: path)
        } catch {
            print("Error when attempting to retrieve data from: \(path.absoluteString). The error was: \(error)")
            return nil
        }
    }
}
