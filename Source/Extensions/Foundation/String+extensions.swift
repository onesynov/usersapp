//
//  String+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

extension String {
    var lastComponent: String? {
        return self.components(separatedBy: Constants.BaseView.dot).last
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,10}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: self)
    }
    
    var isValidUserName: Bool {
        let nameRegEx = "^\\S{1,30}$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return predicate.evaluate(with: self)
    }
    
    var isEmptyWhitespace: Bool {
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }
    
    enum UserApp {
        enum DataUserCell {
            static func new(title: String) -> String {
                return "new " + title.lowercased()
            }
        }
    }
}
