//
//  NSError+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

extension NSError {
    enum UsersApp {
        static func errorWith(description: String) -> NSError {
            let userInfo = [NSLocalizedDescriptionKey: description]
            let error = NSError(domain:"Server", code:0, userInfo:userInfo)
            return error
        }
        
        struct APIClient {
            static let serverError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey :  NSLocalizedString("Server Error", comment: "")])
        }
    }
}
