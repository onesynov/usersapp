//
//  RealmCollection+Notify.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import Foundation
import RealmSwift

public enum NotifyCollectionType {
    case inserted
    case modified
    case deleted
}

// MARK: - Implement extension

extension List: NotifyRealmCollection { }
extension Results: NotifyRealmCollection { }
extension LinkingObjects: NotifyRealmCollection { }

// MARK: - Protocol definition + implementation

public protocol NotifyRealmCollection: class, RealmCollection { }

extension NotifyRealmCollection {
    
    public func notify(when type: NotifyCollectionType, handler: @escaping ((Self) -> Void)) -> NotificationToken {
        return notify(types: [type], handler: handler)
    }
    
    
    private func notify(types: [NotifyCollectionType]?, handler: @escaping ((Self) -> Void)) -> NotificationToken {
        return observe({ [weak self] (change) in
            guard let strongSelf = self else { return }
            
            switch change {
            case .update(_, let deletions,
                         let insertions,
                         let modifications):
                
                guard let types = types else { return handler(strongSelf) }
                
                if types.filter({ $0 == .inserted }).isEmpty == false && !insertions.isEmpty {
                    handler(strongSelf)
                } else if types.filter({ $0 == .modified }).isEmpty == false && !modifications.isEmpty {
                    handler(strongSelf)
                } else if types.filter({ $0 == .deleted }).isEmpty == false && !deletions.isEmpty {
                    handler(strongSelf)
                }
                
            default: break
            }
        })
    }
}
