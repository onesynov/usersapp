//
//  UILabel+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import SDWebImage

extension UIImageView {
    public func image(withString stringUrl: String?) {
        let placeholderImage: UIImage? = nil
        guard let stringUrl = stringUrl, let url = URL(string: stringUrl) else {
            image = placeholderImage
            return
        }
        
        sd_setShowActivityIndicatorView(true)
        
        sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        
        sd_setImage(with: url, placeholderImage: placeholderImage)
    }
}
