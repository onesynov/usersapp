//
//  ViewModelProtocol.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RxSwift

enum State {
    case `default`
    case loading
    case error
}

protocol ViewModelProtocol {
    var state: Variable<State> { get set }
    var successHandler: (() -> Void) { get set }
    var errorHandler: ((Error) -> Void) { get set }
    var parameters: [String: Any] { get }
    
    func load()
}

protocol ViewModelContainableProtocol  {
    associatedtype U
    var models: Array<U> { get }
}

protocol ViewModelPaginationProtocol {
    var page: Int { get }
    var maxPage: Int { get }
    var result: Int { get }
    var canLoad: Bool { get }
    
    func load(page: Int, results: Int)
}

protocol NextViewModelProtocol {
    associatedtype P
    func nextViewModel() -> P
}
