//
//  APIRouter.swift
//  APIRouter
//
//  Created by Oleksandr Nesynov on 15.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import Alamofire
import SwiftyJSON

private struct API {
    struct ProductionServer {
        static let baseURL = "https://randomuser.me/"
    }
    
    struct Parameter {
        struct Key {
            static let api = "api"
            static let page = "page"
            static let results = "results"
            static let seed = "seed"
            static let inc = "inc"
            static let email = "email"
            static let format = "format"
        }
        
        struct Including {
            static let name = "name"
            static let email = "email"
            static let picture = "picture"
            static let login = "login"
            static let phone = "phone"
            static let gender = "gender"
            static let location = "location"
            static let registered = "registered"
            static let dob = "dob"
            static let id = "id"
            static let nat = "nat"
        }
        
        struct Value {
            static let version = "1.2"
            static let empty = ""
            static let comma = ","
            static let abc = "abc"
            static let json = "json"
        }
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

private typealias Inc = API.Parameter.Including
private typealias Key = API.Parameter.Key
private typealias Value = API.Parameter.Value

enum APIRouter: URLRequestConvertible {
    case users(page: Int, results: Int)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .users:
            return .get
        }
    }
    
    // MARK: - apiVersion
    private var apiVersion: String {
        return Value.version
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .users:
            return Value.empty
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case let .users(page: page, results: results):
            return [Key.page: page,
                    Key.results: results,
                    Key.inc: inc,
                    Key.seed: Value.abc,
                    Key.format: Value.json]
        }
    }
    
    // MARK: - Parameters Including
    private var inc: String {
        switch self {
        case .users:
            return [Inc.name,
                    Inc.email,
                    Inc.picture,
                    Inc.login,
                    Inc.phone].joined(separator: Value.comma)
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try API.ProductionServer.baseURL.asURL()
        let apiURL = url.appendingPathComponent(Key.api)
        let apiVersionURL = apiURL.appendingPathComponent(apiVersion).appendingPathComponent(Value.empty)
        
        var urlRequest = URLRequest(url: apiVersionURL)
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)

        return try URLEncoding.default.encode(urlRequest, with: parameters)
    }
}
