//
//  ContactsCell.swift
//  UsersCell
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import DTModelStorage

final class UsersCell: BaseTableViewCell, ModelTransfer {
    @IBOutlet private(set) var fullNameLabel: UILabel!
    @IBOutlet private(set) var phoneLabel: UILabel!
    @IBOutlet private(set) var thumbImageView: UIImageView!
    
    //MARK: - Lifecicle Object
    
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbImageView.roundAllCA()
    }

    //MARK: - ModelTransfer

    func update(with model: UsersCellViewModelProtocol) {
        fullNameLabel.text = model.fullName
        phoneLabel.text = model.phone
        thumbImageView.image(withString: model.imagePath)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.thumbImageView.sd_cancelCurrentImageLoad()
        self.thumbImageView.image = nil
    }
}

protocol UsersCellViewModelProtocol: class {
    var user: User? { get }
    var fullName: String? { get }
    var phone: String? { get }
    var imagePath: String? { get }
    
    init(user: User)
}

final class UsersCellViewModel: UsersCellViewModelProtocol {
    private(set) weak var user: User?
    private(set) var fullName: String?
    
    var phone: String? {
        return user?.phone
    }

    var imagePath: String? {
        return user?.thumbnailImagePath
    }
    
    required init(user: User) {
        self.user = user
        self.fullName = user.firstName.isEmpty ? user.lastName : user.firstName + " " + user.lastName //MARK: - TODO
    }
}
