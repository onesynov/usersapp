//
//  DataUserCell.swift
//  UsersCell
//
//  Created by Oleksandr Nesynov on 14.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import DTModelStorage

final class DataUserCell: BaseTableViewCell, ModelTransfer {
    @IBOutlet private(set) var titleLabel: UILabel!
    @IBOutlet private(set) var dataField: UITextField!

    //MARK: - ModelTransfer

    func update(with model: DataUserCellViewModelProtocol) {
        titleLabel.text = model.title
        dataField.text = model.data
        dataField.placeholder = model.placeholder
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        dataField.text = nil
        dataField.placeholder = nil
    }
}

protocol DataUserCellViewModelProtocol: class {
    var title: String? { get }
    var data: String? { get }
    var placeholder: String? { get }
    init(type: UserCellTypeProtocol)
}

final class DataUserCellViewModel: DataUserCellViewModelProtocol {
    let type: UserCellTypeProtocol
    var title: String? {
        return type.title
    }
    var data: String? {
        return type.data
    }
    var placeholder: String?

    required init(type: UserCellTypeProtocol) {
        self.type = type
        self.placeholder = title == nil ? nil : String.UserApp.DataUserCell.new(title: title!)
    }
}
