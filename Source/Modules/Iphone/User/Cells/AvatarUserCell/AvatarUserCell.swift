//
//  ContactsCell.swift
//  UsersCell
//
//  Created by Oleksandr Nesynov on 14.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import DTModelStorage

final class AvatarUserCell: BaseTableViewCell, ModelTransfer {
    @IBOutlet private(set) var thumbImageView: UIImageView!
    @IBOutlet private(set) var changePhotoButton: UIButton!

    //MARK: - ModelTransfer

    func update(with model: AvatarUserCellViewModelProtocol) {
        thumbImageView.image(withString: model.imagePath)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.thumbImageView.sd_cancelCurrentImageLoad()
        self.thumbImageView.image = nil
    }
}

protocol AvatarUserCellViewModelProtocol: class {
    var imagePath: String { get }
    init(imagePath: String)
}

final class AvatarUserCellViewModel: AvatarUserCellViewModelProtocol {
    let imagePath: String
    
    required init(imagePath: String) {
        self.imagePath = imagePath
    }
}
