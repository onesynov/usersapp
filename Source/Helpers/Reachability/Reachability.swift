//
//  Reachability.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import Alamofire
import RxSwift

class UsersAppReachability {
    static let shared = UsersAppReachability()
    var isReachable: Variable<Bool> = Variable(false)
    private let reachability = NetworkReachabilityManager()
    
    func startListening() {
        reachability?.startListening()
        reachability?.listener = { [unowned self] status in
            self.isReachable.value = self.reachability?.isReachable ?? false
        }
    }
}
