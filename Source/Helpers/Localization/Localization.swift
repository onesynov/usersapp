//
//  Localization.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

enum Localization {
    enum Common {
        static let appName = NSLocalizedString("Common.UsersApp", comment: "Name of the application `UsersApp`")
        static let cancel = NSLocalizedString("Common.Cancel", comment: "Cancel")
        static let send = NSLocalizedString("Common.Send", comment: "Send")
        static let error = NSLocalizedString("Common.Error", comment: "Error")
        static let ok = NSLocalizedString("Common.Ok", comment: "Ok")
        static let no = NSLocalizedString("Common.No", comment: "No")
        static let enter = NSLocalizedString("Common.Enter", comment: "Enter")
    }
    
    enum TabBarController {
        enum TabBarItem {
            static let users = NSLocalizedString("TabBarController.TabBarItem.users", comment: "TabBarController.TabBarItem.users: name tabBarItem users")
            static let saved = NSLocalizedString("TabBarController.TabBarItem.saved", comment: "TabBarController.TabBarItem.users: name tabBarItem saved")
        }
    }
    
    enum UsersViewController {
        static let titleSaved = NSLocalizedString("UsersViewController.title.titleSaved", comment: "UsersViewController screen, title in navigation bar") //"Saved"
        static let titleUsers = NSLocalizedString("UsersViewController.title.titleUsers", comment: "UsersViewController screen, title in navigation bar") //"Users"
    }
    
    enum UserViewController {
        enum UserCell {
            enum Title {
                static let firstName = NSLocalizedString("UserViewController.UserCell.Title.firstName", comment: "UserViewController.UserCell.Title.firstName, text title in label") //"First name"
                static let lastName = NSLocalizedString("UserViewController.UserCell.Title.lastName", comment: "UserViewController.UserCell.Title.firstName, text title in label") //"Last name"
                static let email = NSLocalizedString("UserViewController.UserCell.Title.email", comment: "UserViewController.UserCell.Title.email, text title in label") //"Email"
                static let phone = NSLocalizedString("UserViewController.UserCell.Title.phone", comment: "UserViewController.UserCell.Title.firstName, text title in label") //"Phone"
            }
        }
    }
    
    enum Alers {
        enum User {
            static let enterValidEmail = NSLocalizedString("Alers.User.enterValidEmail", comment: "Alers.User.enterValidEmail: User screen validation `Enter valid email please`")
            static let enterValidName = NSLocalizedString("Alers.User.enterValidName", comment: "Alers.User.enterValidName: User screen validation `Enter valid name please`")
        }
    }
    
    enum Realm {
        static let errorNotification = NSLocalizedString("Realm.UsersViewModel.errorNotification", comment: "Realm.UsersViewModel.errorNotification: Realm error notification")
    }
}
